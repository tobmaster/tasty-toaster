describe('Tasty-Toaster', () => {

    beforeEach(() => {
        cy.clock(null,['setTimeout']);
        cy.visit('/');
        cy.get('tasty-toaster').shadow()
            .as('toaster');     
    });

    it('should grill the toasts', ()=> {        
        cy.get('@toaster')
            .find('.toaster .handle')
            .click();
     
        cy.tick(10000);

        cy.get('@toaster')
            .find('.toasts .toast')
            .eq(0, {timeout: 9000})
            .should('have.class','toasted');

    });

    it('should burn the toasts if gets to hot', ()=> {
        cy.get('@toaster')
            .find('.toaster .handle')
            .click();
     
        cy.tick(10000);

        cy.get('@toaster')
            .find('.toasts .toast')
            .eq(0, {timeout: 9000})
            .should('have.class','toasted');

        cy.get('@toaster')
            .find('.toaster .handle')
            .click();

        cy.tick(10000);

        cy.get('@toaster')
            .find('.fire .particle')
            .should('have.length.gt', 1);
    });



});