import {
  Component,
  Host,
  h,
  Prop,
  State,
  Watch,
  Event,
  EventEmitter,
  Element
} from "@stencil/core";

@Component({
  tag: "tasty-toaster",
  styleUrl: "tasty-toaster.css",
  shadow: true,
})
export class TastyToaster {
  @Element() el: HTMLElement;
  /*
   * number of toasts loaded
   */
  @Prop({ mutable: true, reflect: true }) toasts: number = 0;

  @State() isGrilling: boolean = false;
  @State() crispiness: number = 3;
  @State() isToasted: boolean = false;
  @State() isOnFire: boolean = false;
  @State() heat: number = 0;

  sliderElement!: HTMLInputElement;

  // Event called 'todoCompleted' that is "composed", "cancellable" and it will bubble up!
  @Event({
    eventName: "toasted",
    composed: true,
    cancelable: true,
    bubbles: true,
  })
  toasted: EventEmitter<boolean>;

  @Watch("toasts")
  validateToasts(newValue: number) {
    if (newValue > 3) {
      throw new Error("Maximum capacity is two toasts");
    }
    return newValue;
  }

  private startGrilling() {
    this.isGrilling = true;
    this.heat += this.crispiness;
    console.log('start', this.crispiness);
    if(this.toasted && this.heat > 5) {
      setTimeout(() => {
        this.isOnFire = true;
      }, this.crispiness * 1000);
    }
    setTimeout(() => {
      if(this.isOnFire) {
        this.el.style.setProperty("--crispiness", '0');
      } else {
        this.el.style.setProperty("--crispiness", `${1 - (this.crispiness / 10)}`);
      }
      this.isGrilling = false;
      this.isToasted = true;
    }, this.crispiness * 3000);
  }

  private updateCripiness(e: Event) {
    const slider = e.target as HTMLInputElement;
    this.crispiness = parseInt(slider.value, 10);
    console.log(this.crispiness, slider.value);
  }

  renderToasts() {
    const toastList = [];
    for (let i = 0; i < this.toasts && i < 3; i++) {
      toastList.push(
        <div class={{ toast: true, toasted: this.isToasted }}></div>
      );
    }
    return toastList;
  }

  renderFire() {
    if(!this.isOnFire) {
      return []
    }
    const particles = [];
    for (let i = 0; i < 50; i++) {
      particles.push(
        <div class="particle"></div>
      );
    }

    return particles;
  }

  render() {
    const toastList = this.renderToasts();
    return (
      <Host>
        <div class={{ toaster: true, grilling: this.isGrilling }}>
          <div class="fire">{this.renderFire()}</div>
          <div class="handle" onClick={() => this.startGrilling()}></div>
          <label htmlFor="crispiness">Crispiness</label>
          <input
            class="slider"
            name="crispiness"
            type="range"
            min="1"
            max="5"
            value="3"
            onChange={this.updateCripiness.bind(this)}
            ref={(el) => (this.sliderElement = el as HTMLInputElement)}
          />
          <div class="toasts">{toastList}</div>
        </div>
      </Host>
    );
  }
}
