# tasty-toaster



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description | Type     | Default |
| -------- | --------- | ----------- | -------- | ------- |
| `toasts` | `toasts`  |             | `number` | `0`     |


## Events

| Event     | Description | Type                   |
| --------- | ----------- | ---------------------- |
| `toasted` |             | `CustomEvent<boolean>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
