import { newE2EPage } from '@stencil/core/testing';

describe('tasty-toaster', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<tasty-toaster></tasty-toaster>');

    const element = await page.find('tasty-toaster');
    expect(element).toHaveClass('hydrated');
  });
});
