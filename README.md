
# Tasty Toaster webcomponent test subject

I wanted to learn building a webcomponent with stencil. So I build a toaster.
That tiny little toaster component is very usefull as an example for automated testing,
as it enables exercising how to write tests for webcomponents with a shadow dom,
and as well gives the opportunity for using cy.clock to introduce to timetravel testing. 

Its build on the shoulders of stencil [stencil-app-starter](https://github.com/ionic-team/stencil-app-starter).

## Getting Started

```bash
npm install
npm start
```

To build the component for production, run:

```bash
npm run build
```

